import 'package:flutter/material.dart';

import 'package:flutter_example/screens/home_screen/index.dart';

void main() => runApp(GettingStartedApp());

class GettingStartedApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Getting Started Title",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Simple App"),
          backgroundColor: Colors.red[300],
        ),
        body: HomeScreen(),
      ),
    );
  }
}
