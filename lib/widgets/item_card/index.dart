import 'package:flutter/material.dart';

class ItemCard extends StatelessWidget {
  final Text title;
  final Image image;
  final Widget content;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final Function onClick;
  final BorderRadius radius;
  final double elevation;

  ItemCard({
    title,
    image,
    content,
    margin,
    padding,
    onClick,
    radius,
    elevation,
  })  : title = title ?? null,
        image = image ?? null,
        content = content ?? null,
        elevation = elevation ?? 0,
        onClick = onClick ?? null,
        padding = padding ?? EdgeInsets.fromLTRB(20, 20, 20, 20),
        radius = radius ?? BorderRadius.circular(10.0),
        margin = margin ?? EdgeInsets.all(10);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onClick,
      child: Card(
        elevation: this.elevation,
        margin: this.margin != null ? margin : EdgeInsets.all(10),
        shadowColor: Color.fromRGBO(0, 0, 0, 0.29),
        shape: RoundedRectangleBorder(borderRadius: this.radius),
        child: Padding(
          padding: this.padding,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              if (title != null) Padding(padding: EdgeInsets.fromLTRB(10, 10, 10, 10), child: title),
              image,
              SizedBox(height: 10),
              content,
            ],
          ),
        ),
      ),
    );
  }
}
