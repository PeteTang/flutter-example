import 'package:flutter/material.dart';

import 'package:flutter_example/widgets/item_card/index.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 10,
      itemBuilder: (context, rowNumber) {
        return Container(
          padding: EdgeInsets.all(15.0),
          child: Column(
            children: [
              Center(
                child: ItemCard(
                  elevation: 3.0,
                  image: Image.network("https://i.imgur.com/QrxWATD.png"),
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Out Pet White Duck ",
                        style: TextStyle(fontSize: 18),
                      ),
                      Icon(Icons.thumb_up),
                    ],
                  ),
                ),
              ),
              Divider(color: Colors.black),
            ],
          ),
        );
      },
    );
  }
}
